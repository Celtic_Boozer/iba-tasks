<h1 align = "center"> IBA tasks </h1>
<h2 align = "center">This repository includes my working tasks in IBA.</h2>
<p>This tasks include <b>IBM</b>, <b>z/OS</b> technologies, like <b>REXX</b>
programming language, <b>JCL</b> and others.</p>
<p><b><i>The second task</i></b> includes REXX programming language programs,
and JCL jobs for run it in background mode.</p>

/* The second part of second IBA task REXX */
/* Created by Riabov Dmitry */                        
/* Version 1.1  created 14.01.2018 */                                                              
 
/* This program take from input dataset number and considers count, */
/* of answers, x < y < z < N, N < 1000 */
                     
check = sysdsn(INDD)                                              
if check ^= "OK" then do                                                
    say "No input DS"                                                   
    exit                                                                
end                                                                     
                                                                        
"EXECIO * DISKR INDD (FINIS"                                            
pull string                                                             
say string                                                              
N=word(string,1)                                                        
if datatype(N,'n') = 0 then do                                          
    say "NAN"                                                           
    exit                                                                
end                                                                     
                                                                        
if N < 0 then do                                                        
    say "Negative number"                                               
end                                                                     
                                                                                                                                           
if N>999 then do                                                        
    say "Your number is too big!"                                       
    exit                                                                
end                                                                     
                                                                        
/* First way */
/*count = 0                                                             
do i = N - 1 to 1 by -1                                                 
    do j = N - 2 to 1 by -1                                             
        do k = N - 3 to 1 by -1                                         
            if numbers.i > numbers.j then do                           
                if numbers.j > numbers.k then do                    
                    count = count + 1                               
                end                                                 
            end                                                     
        end                                                         
    end                                                             
end                                                                 
*/                                                                  
                                                                    
/* Second way */
result2 = (N - 1) * (N - 2) * (N - 3) / 6                           
 
/* Push answer to stack and write it into output dataset */                                                                   
push result2                                                        
                                                                    
"EXECIO * DISKW OUTDD (FINIS"                                       
                                                                    
exit                                                                                               
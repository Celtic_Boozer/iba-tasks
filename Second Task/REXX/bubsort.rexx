/* REXX buble sort procedure for the second task IBA */                       
/* Created by Rabau Dzmitry */                        
/* Version 1.3  created 14.01.2018 */                 
 
/* This procedure sort array, by buble sort */
/* To use this procedure you should: */
/* 1. Push to queue your array */
/* 2. Push to stack array's length */
                                                       
pull arrayLength 
                                                                     
do i = 1 to arrayLength by 1                          
    pull x.i                                          
end                                                   
                                                      
buffer = 0                                            
                                                                              
do i = 1 to arrayLength - 1 by 1                      
    do j = 1 to arrayLength - 1 by 1                  
        k = j + 1                                     
        if x.j > x.k then do                          
             buffer = x.j                             
             x.j = x.k                                
             x.k = buffer;                            
        end                                           
    end                                               
end                                                   
                                                            
do i = 1 to arrayLength by 1                          
    queue x.i                                         
end                                                   
                                                      
return ''                                             
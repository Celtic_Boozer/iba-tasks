/* REXX spirally print procedure for the second task IBA */                       
/* Created by Riabov Dmitry */                        
/* Version 1.0  created 17.01.2018 */                                                               

/* This procedure print into output dataset array spirally */
/* To use this procedure you should^ */
/* 1. Pull sorted array to queue */
/* 2. Push to stack array's side size */

pull SZ                                                  
                                                         
do i = queued() - 1 to 0 by -1                           
    pull num.i                                           
end                                                      
                                                         
padding = 0                                              
i = 0                                                    
j = 0                                                    
m = 0                                                    
                                                         
/* Spiral matrix */                                      
do while padding < SZ                                    
                                                         
    do while j < SZ - padding                            
        spiral.i.j = num.m                               
        j = j + 1                                        
        m = m + 1                                        
    end                                                  
                                                         
    i = i + 1                                            
    j = j - 1                                            
                                                         
    do while i < SZ - padding                            
        spiral.i.j = num.m                               
        i = i + 1                                        
        m = m + 1                                        
    end                                                  
                                                         
    i = i - 1                                            
    j = j - 1                                            
                                                         
    do while j >= padding                                
        spiral.i.j = num.m                               
        j = j - 1                                        
        m = m + 1                                        
    end                                                  
                                                         
    i = i - 1                                            
    j = j + 1                                            
                                                         
    do while i > padding                                 
        spiral.i.j = num.m                               
        i = i - 1                                        
        m = m + 1                                        
    end                                                  
                                                         
    i = i + 1                                            
    j = j + 1                                            
                                                         
    padding = padding + 1                                
                                                         
end                                                      
                                                                  
spiralString. = ""                                       
do i = 0 to SZ - 1 by 1                                  
    do j = 0 to SZ - 1 by 1                              
        spiralString.i = spiralString.i spiral.i.j                                             
    end                                                  
end                                                      
                                                         
do i = 0  to SZ - 1 by 1                                 
    queue spiralString.i                                 
end                                                      
                                                         
"EXECIO * DISKW OUTDD (FINIS"                            
                                                         
return ''                                                
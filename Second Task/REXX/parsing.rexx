/* REXX parsing procedure for the second task IBA */
/* Created by Riabov Dmitry */                        
/* Version 1.2  created 12.01.2018 */
 
/* This procedure read text from input dataset and parse numbers, */
/* all other symbols are ignored */                                                                                  
                                                                        
/* To use this procedure u should: */                                   
/* 1. Push all strings from file to queue.*/                            
/* 2. Push number of strings to stack */                                
                                                                        
pull numberStrings                                                      
                                                                        
do i = 1 to numberStrings by 1                                          
                                                                        
    pull strings.i                                                      
                                                                        
    strings.i = substr(strings.i,1,72)                                  
    strings.i = strip(strings.i)                                                                
    lengthStr.i = length(strings.i)
                                
    l = 1                                                               
    k = 0                                                               
 
/* Loop starts from the last symbol in string */                   
/* If current symbol - is a number and the previous symbol too */   
/* Number = current * 10^l (l - itterate) + previous; l++;*/        
/* If previous symbol - NaN: Number = current; l++; */              
/* If current - NaN ingnore this symbol, l = 1; */                  
                                                                        
do j = lengthStr.i to 1 by -1                                           
        if datatype(substr(strings.i, j, 1),'n') = 1 then do            
            if datatype(substr(strings.i, j + 1, 1),'n') = 0 then do    
                k = k+1                                                 
                numbers.i.k = substr(strings.i, j, 1)                   
                l = 1                                                   
            end 
 
            if datatype(substr(strings.i, j + 1, 1),'n') = 1 then do   
                numbers.i.k = numbers.i.k+substr(strings.i, j, 1)*10**l
                l = l+1                                                
            end                                                        
        end                                                            
        if substr(strings.i, j, 1) = '-' then do                       
                numbers.i.k = numbers.i.k*-1                           
        end                                                            
        quantity.i = k                                                 
    end                                                                
end                                                                    
                                                                        
do i = 1 to numberStrings by 1                                         
    do j = 1 to quantity.i by 1                                        
        queue numbers.i.j                                              
    end                                                                
end                                                                    
                                                                        
do i = 1 to numberStrings by 1                                         
    push quantity.i                                                    
end                                                                    
                                                                       
return ''                                                                                                                      
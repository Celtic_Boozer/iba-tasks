/* This is main file of the second task, first part on REXX */
/* Created by Riabov Dmitry */                        
/* Version 1.8  created 25.01.2018 */          
 
/* This procedure takes from input dataset square matrix, sort it, */
/* and print it spirally to outpu dataset */                                                                       
 
"MAKEBUF"                                                              
                                                               
"EXECIO * DISKR INDD (FINIS"                
                                                                     
numberStrings = queued()                                               
push numberStrings                                                     
call parsing                                                           
                                                                       
do i = 1 to numberStrings by 1                                         
    pull quantity.i                                                    
end                                                                    
                                                                        
do i = 1 to numberStrings by 1                                         
    do j = 1 to quantity.i by 1                                        
        pull numbers.i.j                                               
    end                                                                
end                                                                    
                                                                       
"DROPBUF"                                                              
                                                                      
do i = 1 to numberStrings by 1                                         
    if numberStrings ^= quantity.i then do                             
        say "Your matrix is not square!"                               
        exit                                                           
    end                                                                
end                                                                                                                
 
"MAKEBUF"                                                              

arrayLength = 0                                                        
 
do i = 1 to numberStrings by 1                                         
    arrayLength = arrayLength + quantity.i                             
    do j = 1 to quantity.i by 1                                        
        queue numbers.i.j                                              
    end                                                                
end                                  
 
push arrayLength                     
 
call bubsort                         
                                  
do i = 0 to queued() - 1 by 1        
    pull sorted.i                    
end                                  
 
"DROPBUF"                            
                                     
"MAKEBUF"                            
                                     
do i = 0 to arrayLength - 1 by 1         
    queue sorted.i                   
end                                  
                                     
push quantity.1                      
                                     
call spiral                          
                                     
"DROPBUF"                            
 
exit                                     
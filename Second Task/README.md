<h1 align = "center">The second task</h1>
 
<h2 align = "center">The first part of the second task</h2>
 
<p> <b>Task:</b> we should to make <b>square matrix</b> in external dataset,
read it in the <i><b>REXX</b></i> program, sort it and write into the external 
dataset sorted array spirally by ascending. Work with datasets should be organized 
by <b><i>JCL</i></b>.</p>
 
<h3>File listing REXX folder</h3>
<ul>
	<li><i><b>Main</b></i> file is the main file of program, we run it in JCL</li>
	<li><i><b>Bubsort</b></i> file used for sorting array</li>
	<li><i><b>Parsing</b></i> file parse matrix from input dataset.</li>
	<li><i><b>Spiral</b></i> file write matrix into the output dataset spirally</li>
</ul>
 
<h3>File listing JCL folder</h3>
 
<ul>
	<li><i><b>Sortjod</b></i> submit this job to run a program</li>
</ul>
 
<p>If you need to <b>change</b> output/input dataset u should to edit
sortjob, and change <b>jobcard</b>, and all dataset names on your.</p>
 
<h2 align = "center">The second part of the second task</h2>
 
<p><b>Task:</b> we should to make program on <b><i>REXX</i></b> that's 
take from input dataset number and considers count, of answers, 
<b>x < y < z < N, N < 1000</b>. Work with datasets should be organized 
by <b><i>JCL</i></b>.</p>
 
<h3>File listing REXX folder</h3>
<ul>
	<li><i><b>Second</b></i> file is the main file of program, we run it in JCL</li>
</ul>
 
<h3>File listing JCL folder</h3>
 
<ul>
	<li><i><b>Countjob</b></i> submit this job to run a program</li>
</ul>
 
<h2 align = "center">FAQ</h2>
 
<p>Your REXX programs and JCL jobs should be alocated in <b>LIBRARY</b> dataset</p>
 
<p>If you want to run this programs in <b>background</b> enter <i>"SUBMIT [FILENAME]"</i>
in <b>ISPF/PDF panel</b>.</p>
 
<p>If you want to run this programs in <b>foreground</b> change INDD and OUTDD in your
program on dataset names, and write check's on exist OUTDD dataset.
After, in ISPF/PDF panel enter <i>"TSO EX 'FILENAME' EXEC"</i>.</p>